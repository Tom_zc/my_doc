---
标题:     community仓库sig-info.yaml新增版本看护者
类别:     流程设计
摘要:     sig-info.yaml新增版本看护者字段
作者:     朱超 (chaotomzhu at gmail.com)
状态:     活跃
编号:     oEEP-0016
创建日期:  2024-01-24
修订日期:  2024-01-24
---

## 动机/问题描述:

### 1.需求

目前Kernel Sig向Infrastructure Sig提出关于approve pr的需求：
1. 更改pr approve代码合入权限逻辑：由现有的: Maintainers+Committers才有权限对PR进行approve  改为:   Maintainers+指定分支版本看护者才有权限对PR进行approve。   

2.  版本看护者branch keeper必须是committers中之一。

3. 如果没有设置版本看护者branch keeper, 则依旧是使用现有逻辑处理：Maintainers+Committers才有权限对PR进行approve。

   现有approve合入逻辑：
   
   ![1706079949342](assets/1706079949342.png)
   
   更改后approve合入逻辑：
   
   ![1706080084625](assets/1706080084625.png)

### 2.本oEEP解决的问题

- 问题1：统一分支版本设置对应的branch-keeper去维护。

## 方案的详细描述:
### 1.配置branch-keeper

在sig-info.yaml文件增加branches配置，如下图所示：

~~~yaml
branches:
  - repo_branch:
      - repo: openeuler/kernel
        branch: openEuler-22.03-LTS-SP1
      - repo: src-openeuler/kernel
        branch: openEuler-22.03-LTS-SP3
    keeper:
      - gitee_id: zhangjialin11
~~~

### 2.后台实现逻辑

![1706080587987](assets/1706080587987.png)

### 3.评估影响

对现状是否由影响？暂无发现影响点

1.验证修改sigs/kernel/sig-info.yaml文件后，获取该文件的其他服务是否有影响？
答：统计ci-robot等相关服务，排除owner-file-cache、robot-gitee-openeuler-welcome等项目，暂无发现影响点。

2.是否会对没有设置branch_keeper的approve处理有影响？
答：如果没有设置版本看护者branch keeper, 则依旧是使用现有逻辑处理：Maintainers+Committers才有权限对PR进行approve。

### 4.后续推广

如果有相关sig组感兴趣，或者想使用branch_keeper来看护版本分支？

1.只需要在sig-info.yaml文件增加相应的版本看护者，配置模板如下所示：

~~~yaml
branches:
  - repo_branch:
      - repo: openeuler/kernel
        branch: openEuler-22.03-LTS-SP1
      - repo: src-openeuler/kernel
        branch: openEuler-22.03-LTS-SP3
    keeper:
      - gitee_id: zhangjialin11
~~~

