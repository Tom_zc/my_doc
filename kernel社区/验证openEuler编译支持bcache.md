### 验证openEuler编译内核支持bcache

### 1.编译内核

#### 1.获取内核源码

1. 下载源码包

   ~~~bash
   yum install git
   cd /root/ && git clone https://gitee.com/openeuler/kernel.git
   cd /root/kernel && git checkout OLK-5.10
   源码详细链接: https://gitee.com/openeuler/kernel/tree/OLK-5.10/kernel
   ~~~

2. 配置镜像源。 如果编译系统本身是openEuler-22.03-lts-sp1-everything-x86_64-dvd.iso， 则不需要进行以下操作。

   1. 下载iso文件

      ~~~bash
      wget https://mirrors.nju.edu.cn/openeuler/openEuler-22.03-LTS-SP1/ISO/x86_64/openEuler-22.03-LTS-SP1-x86_64-dvd.iso
      ~~~

   2. 创建一个本地文件夹用于挂载本地镜像。

      ~~~bash
      mkdir -p /iso
      ~~~

   3. 将iso文件挂载到本地文件夹。

      ~~~bash
      mount /root/openEuler-22.03-LTS-SP1-x86_64-dvd.iso /iso
      ~~~

   4. 创建镜像yum源。

      ~~~bash
      vi /etc/yum.repos.d/openEuler.repo
      ~~~

      在文件中加入以下内容：

      ~~~bash
      [Base]
      name=Base
      baseurl=file:///iso
      enabled=1
      gpgcheck=0
      priority=1
      
      [openEuler-source]
      name=openEuler-source
      baseurl=http://repo.openeuler.org/openEuler-22.03-LTS-SP1/source/
      enabled=1
      gpgcheck=1
      gpgkey=http://repo.openeuler.org/openEuler-22.03-LTS-SP1/source/RPM-GPG-KEY-openEuler
      
      [openEuler-os]
      name=openEuler-os
      baseurl=http://repo.openeuler.org/openEuler-22.03-LTS-SP1/OS/aarch64/
      enabled=0
      gpgcheck=1
      gpgkey=http://repo.openeuler.org/openEuler-22.03-LTS-SP1/OS/aarch64/RPM-GPG-KEY-openEuler
      
      [openEuler-everything]
      name=openEuler-everything
      baseurl=http://repo.openeuler.org/openEuler-22.03-LTS-SP1/everything/aarch64/
      enabled=0
      gpgcheck=1
      gpgkey=http://repo.openeuler.org/openEuler-22.03-LTS-SP1/everything/aarch64/RPM-GPG-KEY-openEuler
      
      [openEuler-EPOL]
      name=openEuler-epol
      baseurl=http://repo.openeuler.org/openEuler-22.03-LTS-SP1/EPOL/aarch64/
      enabled=1
      gpgcheck=0
      
      [openEuler-update]
      name=openEuler-update
      baseurl=http://repo.openeuler.org/openEuler-22.03-LTS-SP1/update/aarch64/
      enabled=1
      gpgcheck=0
      ~~~

3. 安装依赖。

   ~~~bash
   dnf -y install ncurses-devel bison m4 flex rpm-build rpmdevtools asciidoc audit-libs-devel binutils-devel elfutils-devel elfutils-libelf-devel gtk2-devel java-1.8.0-openjdk-devel xz-devel libbabeltrace-devel libunwind-devel newt-devel numactl-devel openssl-devel pciutils-devel perl-generators python3-devel python3-docutils xmlto zlib-devel createrepo genisoimage make gcc
   ~~~
   
   注意如果在虚拟机virtualbox上编译，需要安装
   
   ~~~bash
   yum install dwarves
   ~~~

#### 2.修改内核配置

1. 获取内核配置文件。

   ~~~bash
   cd /root/kernel
   cp arch/x86/configs/openeuler_defconfig .config    # x86
   cp arch/arm64/configs/openeuler_defconfig .config  # arm
   ~~~

2. 配置Bcache模块。

   ~~~bash
   make menuconfig
   ~~~

   a.选择“Device Drivers ”。

   ![1683794885090](assets/1683794885090.png)

   b.按“Enter”键进入下一级菜单，选择“Multiple device driver support (RAID and LVM )”。

   ![1683794942319](assets/1683794942319.png)

   c.按“Enter”键进入下一级菜单，选中“Block device as cache”，键盘输入“M”选中该配置。

   ![1683795035480](assets/1683795035480.png)

   d.按两次“exit”返回至第一层。

   ![1683795066725](assets/1683795066725.png)

   e.保存,点击“yes”保存配置。

   ![1683795763236](assets/1683795763236.png)

   

4. 确认配置文件。

   a.确认Bcache模块打开。

   ![1683872563379](assets/1683872563379.png)

   

5. 覆盖默认配置文件。

   ~~~bash
   cp .config arch/x86/configs/openeuler_defconfig -y
   或者执行以下指令：make update_oedefconfig
   ~~~

#### 3.编译

1.编译。

~~~bash
cd /root/kernel
make -j16
~~~

![1683872632830](assets/1683872632830.png)

2.安装。

~~~bash
make -j modules_install
make install
~~~

![1683872660850](assets/1683872660850.png)

3.更新grub启动列表。

~~~bash
grub2-mkconfig -o /boot/efi/EFI/openEuler/grub.cfg
~~~

![1683872696760](assets/1683872696760.png)

7.修改默认内核启动项目。

~~~bash
cat /etc/grub2.cfg  | grep openEuler
grub2-set-default 'openEuler (5.10.0+) 22.03 (LTS-SP1)'
~~~

![1683872758960](assets/1683872758960.png)

8.重启系统。

~~~bash
reboot
~~~

![1683872921253](assets/1683872921253.png)



### 2.使用bcache-tools验证

#### 1.克隆项目

~~~bash
rpmdev-setuptree
cd /root && git clone https://gitee.com/src-openeuler/bcache-tools.git
cp /root/bcache-tools/bcache-tools-1.1.tar.gz /root/rpmbuild/SOURCES/
cp /root/bcache-tools/bcache-tools.spec /root/rpmbuild/SPECS/
~~~

#### 2.编译和安装

~~~bash
rpmbuild -ba /root/rpmbuild/SPECS/bcache-tools.spec
dnf install /root/rpmbuild/RPMS/x86_64/bcache-tools-1.1-3.x86_64.rpm
~~~

#### 3.make-bcache制作缓存盘

~~~bash
make-bcache -B /dev/sdb -C /dev/sdc
~~~

![1683873018929](assets/1683873018929.png)

