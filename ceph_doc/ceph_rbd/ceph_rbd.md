## 1.rbd命令

1> 卷的增删查改:

~~~bash
增： rbd create rbd1 --size 1024 -p p1
删： rbd rm p1/rbd1
查信息： rbd info p1/rbd1
查容量： rbd du p1/rbd1
修改容量:  rbd resize rbd1 --size 500M -p p1
修改名称:  rbd rename p1/rbd1 p1/rbd10
修改qos：rbd config image set p1/rbd10 rbd_qos_bps_limit
设置qos: rbd config image get p1/rbd10 rbd_qos_bps_limit
~~~

2> 快照的增删查改

~~~bash
创建快照:rbd snap create {pool-name}/{image-name}@{snap-name}
查看快照:rbd snap ls {pool-name}/{image-name}
快照回滚:rbd snap rollback {pool-name}/{image-name}@{snap-name}
删除快照：rbd snap rm {pool-name}/{image-name}@{snap-name}
快照保护：rbd snap protect {pool-name}/{image-name}@{snapshot-name}
取消保护：rbd snap unprotect {pool-name}/{image-name}@{snapshot-name}
快照克隆：rbd clone rbd/foo@snapname rbd/bar
快照flatten: rbd flatten {pool-name}/{image-name}
~~~

3>卷的一致性组

~~~bash
rbd group create
Create a group.

rbd group ls [-p | –pool pool-name]
List rbd groups.

rbd group image add
Add an image to a group.

rbd group image list
List images in a group.

rbd group image remove
Remove an image from a group.

rbd group rename
Rename a group. Note: rename across pools is not supported.

rbd group rm
Delete a group.

rbd group snap create
Make a snapshot of a group.

rbd group snap list
List snapshots of a group.

group snap rm
Remove a snapshot from a group.

group snap rename
Rename group’s snapshot.

group snap rollback
Rollback group to snapshot.
~~~

3> rbd查看组信息

~~~bash
$ rados df  #　查看存储池
$ rbd ls pool_name　#　查看存储池下有那些卷
$ rbd info pool_name/image_name　#　查看卷的具体信息
$ rados ls -p pool_name|grep 37dfbd87752d(rbd_id)
$ rados -p pool_name listomapvals rbd_header.37dfbd87752d
$ rados -p pool_name listomapvals rbd_group_directory
$ rados -p pool_name listomapvals rbd_group_header.40ada8fa7e2
~~~



## 2.卷通过iscsi目标器挂载

~~~bash
安装
yum install iscsi-initiator-utils
配置客户端
vim /etc/iscsi/initiatorname.iscsi
重启服务
systemctl restart iscsid
发现目标器：
iscsiadm -m discovery -t st -p 10.0.100.12
登录
iscsiadm -m node -T iqn.2021-03.com.xitcorp.iscsi-gw:3527e937b9e9097d -p 10.0.100.12 -l
登出：
iscsiadm -m node -T iqn.2021-03.com.xitcorp.iscsi-gw:3527e937b9e9097d -p 10.0.100.12 -u
查看是否连接上：
iscsiadm -m session -P 3|grep Attached
挂载：
mkfs.xfs /dev/sdb
mount /dev/sdb /mnt/test
比对数据一致性：
find ./sde -type f -print0|xargs -0 md5sum|sort > sde_md5.txt
find ./sdf -type f -print0|xargs -0 md5sum|sort > sdf_md5.txt
diff命令也可以查看
diff sde_md5.txt sdf_md5.txt 
~~~



