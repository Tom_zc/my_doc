## ceph rgw

## 1.rgw对象服务

Ceph通过radosgw提供RESTFUL HTTP API接口支持对象存储能力，radosgw构建在librados之上，兼容Amazon S3以及Opensack Swift。

![1598457064140](assets/1598457064140.png)

radosgw使用Civetweb作为其Web Sevice/默认使用端口7480,本质上是一个客户端程序，提供FastCGI 服务。提供服务作·为一个客户端程序，需要满足如下要求：

```bash
·一个实例名称，默认为：gateway
· 一个合法用户
· 多个存储池
· 一个数据目录
· 在ceph.conf中添加一个配置项
· 前端配置文件
```

radosgw支持以Apache、Civetweb、Nginx作为前端。Civetweb是默认前端，通过修改ceph.conf配置文件能够很容易的替换为Apache，通过配置能也以nginx作为前端。官方文档：http://docs.ceph.org.cn/start/quick-rgw/

### 1.Civetweb默认端口开放或者关闭端口服务

```bash
# Civetweb 默认使用 7480 端口。
firewall-cmd --zone=public --add-port=7480/tcp --permanent
firewall-cmd --reload
firewall-cmd --zone=public --list-all
```

### 2.安装 CEPH 对象网关

```bash
$ cd /etc/ceph
# ceph-deploy install --no-adjust-repos --rgw <client-node> [<client-node> ...]
$ ceph-deploy install --no-adjust-repos --rgw node0010
(注意这里要加上--no-adjust-repos防止原本配置好的源文件 (/etc/yum.repos.d/ceph.repo) 自动被ceph安装程序该变)
```

### 3.新建 CEPH 对象网关实例

```bash
# 从管理节点的工作目录，在 client-node 上新建一个 Ceph 对象网关实例。
$ cd /etc/ceph
# ceph-deploy rgw create <client-node> [<client-node> ...]
$ ceph-deploy --overwrite-conf rgw create node10
```

### 4.配置CEPH 对象网关实例

```bash
# 通过修改 Ceph 配置文件可以更改默认端口（比如改成 8899 ）,在 [global] 节后增加一个类似于下面的小节：
# rgw_frontends:rgw的前端配置，
# 一般配置为使用轻量级的civetweb；prot为访问rgw的端口，根据实际情况配置；
# num_threads：为rgw的civetweb的线程数
# gw_thread_pool_size：rgw前端web的线程数，与rgw_frontends中的num_threads含义一致，但num_threads 优于rgw_thread_pool_size的配置，两个只需要配置一个即可。

[client.rgw.node10]
rgw_frontends = civetweb port=7480 num_threads=1500

# 修改完后重启服务:
systemctl restart ceph-radosgw@rgw.node-name.service
systemctl restart ceph-radosgw@rgw.node10.service
# 可以通过检查7480端口号来检查
```

### 5.创建用户

\# 为了使用 REST 接口，首先需要为S3接口创建一个初始 Ceph 对象网关用户。然后，为 Swift 接口创建一个子用户。然后你需要验证创建的用户是否能够访问网关。

```bash
radosgw-admin user create --uid="此处填用户名" --display-name="此处填名字"
radosgw-admin user create --uid="test" --display-name="haifeng"
# 注意：创建完用户后请将用户信息保存下来
```

### 6.授权用户

```bash
radosgw-admin caps add --uid=test --caps="users=*"
radosgw-admin caps add --uid=test --caps="buckets=*"
radosgw-admin caps add --uid=test --caps="metadata=*"
radosgw-admin caps add --uid=test --caps="usage=*"
```

### 7.新建一个 SWIFT 用户并创建密钥（非必须）

```bash
# 新建一个swift用户
radosgw-admin subuser create --uid=testuser --subuser=testuser:swift --access=full
# 新建 secret key:要记住（非必须）
sudo radosgw-admin key create --subuser=testuser:swift --key-type=swift --gen-secret
```

### 8.查看用户信息

当创建完用户后如果忘记用户密钥，可以进行查看：

radosgw-admin user info --uid=test



## 2.探讨方向

1.实现keepavlie+nginx/tengine的高可用，高可靠。





