## 1.ceph_fs

### 1.部署ceph-mds

1.部署mds

```bash
ceph-deploy mds create node10:node10-0
systemctl start ceph-mds@node10-0
```

2.mds的相关指令

+ 引用MDS守护程序的大多数管理命令都接受灵活的参数格式，该格式可以包含等级，GID或名称。在使用等级的情况下，可以选择使用领先的文件系统名称或ID进行限定 。如果守护程序是备用守护程序（即当前未为其分配等级），则只能通过GID或名称来引用它。

+ 管理故障转移, 如果MDS守护程序停止与监视器通信，则监视器将等待 `mds_beacon_grace`几秒钟（默认为15秒），然后将该守护程序标记为 *laggy*。如果有备用服务器，监视器将立即替换滞后的守护程序。每个文件系统都可以指定许多待命守护程序，使其视为正常。该数字包括等待重排失败的备用重放中的守护程序（请记住，不会将备用故障守护程序指定为接管另一个级别的故障或另一个CephFS文件系统中的故障）。不在重播中的备用守护程序池计入任何文件系统计数。每个文件系统可以使用以下命令设置所需的备用守护程序数量：

  ~~~bash
  $ ceph fs set <fs name> allow_standby_replay <bool>
  ~~~

### 2.创建文件系统

```bash
#创建存储池并创建应用
$ ceph osd pool create cephfs_data <pg_num> replicated|erasure crush_rule 1
$ ceph osd pool create cephfs_metadata <pg_num> replicated|erasure crush_rule 1
$ ceph osd pool application enable cephfs_metadata cephfs --yes-i-really-mean-it
$ ceph osd pool application enable cephfs_data cephfs --yes-i-really-mean-it

$ ceph fs new cephfs cephfs_metadata cephfs_data
$ ceph fs ls
$ ceph fs status
$ ceph fs dump
```

注意：将纠删码存储池与CEPHFS一起使用，您可以纠删码存储池用作CephFS数据池，只要它们启用了覆盖即可，操作如下：

```bash
$ ceph osd pool set my_ec_pool allow_ec_overwrites true
```

请注意:
1.仅当OSDS与BlueStore后端一起使用时才支持EC纠删码编码。
2.您不得将纠删码编码池用作CephFS元数据池，因为CephFS元数据是使用EC池无法存储的RADOS *OMAP*数据结构存储的。

4.挂载文件系统

```bash
1.通过ceph挂载命令：
$ mount -t ceph 192.168.57.3:6789:/ /mnt/test/ -o name=admin,secret=AQBsQsVelMJrLhAAU5znbuOfmlO9NNEsO6NVkg==

2.通过linux自带的客户端mount.ceph
$ mount -t ceph 1.2.3.4:/ mountpoint
卸载命令：
$umount /mnt/test/
```

5.mds的状态描述

```bash
up:active：正常运行的节点
up:standby：热备的节点
up:standby_replay：冷备的节点
```

6.常用命令

```bash
$ ceph fs fail <fs name>:删除文件系统
$ ceph fs rm <fs name> --yes-i-really-mean-it:删除文件系统

$ ceph fs get <fs name> ：获取文件系统的信息
$ ceph fs set <fs name> var val：设置文件系统的信息

$ ceph fs add_data_pool <fs name> pool_name: 将存储池添加到文件系统中的数据池
$ ceph fs rm_data_pool <fs name> pool_name: 将存储池从文件系统中的数据池中删除

$ ceph fs set <fs name> max_file_size <size in bytes>：设置最大文件大小，64位字段，1TB
$ ceph fs set <fs name> down true:通过down标志来关闭cephfs集群
$ ceph fs set <fs name> down false:通过down标志来打开cephfs集群
```

7.快速关闭集群进行删除或者灾难恢复

```bash
$ ceph fs fail <fs name>
$ ceph fs set <fs name> joinable false: 关闭文件系统
$ ceph fs set <fs name> joinable true: 恢复文件系统
```

8.命令查看最大文件夹数和最大容量数

```bash
$ ceph setfattr -n ceph.quota.max_bytes -v 100000000 /some/dir     # 100 MB
$ ceph setfattr -n ceph.quota.max_files -v 10000 /some/dir  
$ ceph getfattr -n ceph.quota.max_bytes /some/dir
$ ceph getfattr -n ceph.quota.max_files /some/dir
```

9.查看客户端连接：

```bash
手动检查客户端：
ceph tell mds.node0010-0 client ls
可以使用属性来查看它：
ceph tell mds.0 client evict id=4305

查看是否进入黑名单：
ceph osd blacklist ls
删除黑名单
ceph osd blacklist rm 192.168.57.3:6808/401586748
取消黑名单设置：
mds_session_blacklist_on_timeout = false

查看慢请求的命令：
1.rados问题
2.运行比较旧的客户端
3.mds处理不过来，可能与缓存有关系。
使用以下命令进行查看：
ceph daemon mds.node0010-0 dump_ops_in_flight
```

### 3.网关方式共享

探讨方向？

~~~bash
1.通过nfs-ganesha导出,
2.通过samba导出，并对接ad,ldap等域服务器
3.通过vsftpd导出
4.通过ctdb+lvs实现高可用。
~~~

