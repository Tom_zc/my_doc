## 背景

目前sds sig致力于丰富ceph组件生态，在openEuler 22.03-lts sp1版本中，ceph使用bcache对osd进行数据加速，使用时发现bcache-tools工具在/dev下找不到bcache0文件，经定位发现x86架构没有加载bcache内核模块，希望kernel sig支持x86架构加载bcache模块。 

## 需求

ceph中使用bcache进行osd加速是提升性能的常用手段之一

详细见：

http://mysrc.cn-bj.ufileos.com/wuhan/06_%E4%BD%BF%E7%94%A8Bcache%E4%B8%BACeph%20OSD%E5%8A%A0%E9%80%9F%E7%9A%84%E5%85%B7%E4%BD%93%E5%AE%9E%E8%B7%B5%20by_%E8%8A%B1%E7%91%9E.pdf

提升性能对比：

![1680833434268](assets/微信截图_20230407094823.png)

详细见链接：https://www.bilibili.com/video/BV1i14y1W76u/?spm_id_from=333.788&vd_source=f7050ed1d78b437c78f09935d9d1a633

## 后期输出

1.基于sds sig的issue： https://gitee.com/openeuler/sds_doc/issues/I6TIVB?from=project-issue

2.输出基于openEuler x86的bcache osd加速的指导说明书(包括使用说明，测试对比)

## 实现

经刘志强分析发现：只需要修改宏配置，暂不涉及依赖。

![1680844249161](C:\Users\86182\AppData\Roaming\Typora\typora-user-images\1680844249161.png)

![1680844284019](C:\Users\86182\AppData\Roaming\Typora\typora-user-images\1680844284019.png)
