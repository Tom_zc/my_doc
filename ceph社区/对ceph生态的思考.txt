一、社区的现有建设
1.社区的愿景和目标
	# sig-SDS 愿景
	愿景：汇聚存储英才，共同打造高质量、高性能、高可靠性的分布式存储组件版本，构建丰富的南北向生态

	# 目标
	- 紧跟原生社区，坚持“upstream first”原则，丰富ARM/openEuler分布式存储组件生态圈
	- 针对ARM架构开展优化，打造极致性能
	- 为openEuler发布版本的组件，决策版本、打造质量防护网，实现高质量发布

2.建立生态，目前已经适配的版本
	openEuler1.0                       ---> ceph 12.2.8
	openEuler1.0  Base                 ---> ceph 12.2.8
	openEuler 20.03-lts                ---> ceph 12.2.8
	openEuler 20.03-lts-sp1 	     ---> ceph 12.2.8
	openEuler 20.03-lts-sp2            ---> ceph 12.2.8
	openEuler 20.03-lts-sp3            ---> ceph 12.2.8
	openEuler 20.09                    ---> ceph 12.2.8
	openEuler 21.03                    ---> ceph 14.2.15
	openEuler 21.09                    ---> ceph 14.2.15
	openEuler 22.03-lts                ---> ceph 16.2.7
	openEuler 22.03-lts-sp1            ---> ceph 16.2.7

3.实时跟进上游社区动态，并发布ceph原生社区动态。

4.开展特性开发
    1>鲲鹏硬件加速器ceph使能指南

    2>ceph国密使能

    3>ceph集群关机、开机的自动化实现

    4>优化ceph性能特性


二、对ceph生态的思考
具体措施如下:
1.基于目前社区主推的版本，扩展生态，在正式版本中推出ceph所涉及的包, 可以用ceph16.2.7来适配
	1> 从各个模块来分析
		部署：
		ceph-deploy、ceph-ansible

		服务器：
		prometheus、node_exporter

		硬盘
		smartmontools、pyudev、sysstat、bcache-tools
		服务器阵列管理工具: perccli和storcli

		卷：
		ceph-iscsi、python3-rtslib、targetcli、tcmu-runner

		文件：
		samba(ctdb)、nfs-ganesha 、vsftpd

		对象：
		nginx/tegine、keepalive

	2>目前存在的问题点：
		1.引入了相关包，但是并没有在正式版本中推出，比如说: ceph-deploy、ceph-iscsi、ceph-ansible等，建议后期全部推到正式版本中。
		2.还有一些包没有引入，比如说nfs-ganesha。

2.对接k8s, openstack
	基于openEuler的ceph16版本出对接openstack的指导说明书
	基于openEuler的ceph16版本出对接k8s的指导说明书
	思考：基于其他sig组的技术给ceph注入活力，实现创新


3.双手拥抱sds爱好者和各大厂商
	1>.开源ceph和正式上线环境还有一步距离，填好这一步距离。
		1.输出基于ceph16的环境配置，安装、部署，块、文件、对象等详细文档；文档的实现必须满足生成的需求，满足高可用。
			1.建议在openEuler源码仓下创建项目ceph_doc管理这些文档；完善文档，推动更多人参与，提高社区曝光度，形成反馈机制，生成新需求，持续需求推动上游社区建设。
			2.针对于特定的需求，可以制作工具，如果工具成熟，反馈好评，可以反推到上游社区。
		2.性能优化参数的配置讲解。
	

	2>.开展一些解决方案特性的调研，
		比如说: 卷的远程复制和恢复，数据的一致性检查scrub等。

4.增加测试工具的使用指导: Vdbench 、CosBench

三、正在处理
1.ceph相关技术文档的整理归档：https://gitee.com/src-openeuler/ceph/issues/I6GJIT?from=project-issue
2.自己之前的一些总结：https://gitee.com/Tom_zc/my_doc/tree/master/ceph_doc

三、总结
紧跟上游ceph社区，以为openstack和k8s提供存储能力为基石, 双手拥抱sds爱好者和各大厂商。












