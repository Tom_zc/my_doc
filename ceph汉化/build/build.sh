#!/usr/bin/env bash
cd /root && mkdir ceph
cd /root/ceph && wget https://download.ceph.com/tarballs/ceph-16.2.7.tar.gz && tar -zxvf ceph-16.2.7.tar.gz
cd ceph-16.2.7/src/pybind/mgr/dashboard/frotend && npm i -f
npm run build:localize