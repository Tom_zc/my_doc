# -*- coding: utf-8 -*-
# @Time    : 2024/4/25 15:29
# @Author  : Tom_zc
# @FileName: python17.py
# @Software: PyCharm

import click
import requests
import csv
import re

open_status = "open"
progress_status = "progressing"

url = "https://gitee.com/api/v5/repos/openharmony/docs/issues?access_token={}&state={}&sort=created&direction=desc&page={}&per_page={}"
pr_url = "https://gitee.com/openharmony/docs/pulls/{}.diff"


def get_total_font(pr_number):
    total_font = 0
    request_url = pr_url.format(pr_number)
    ret = requests.get(request_url)
    lines = ret.content.decode("utf-8").split("\n")
    for line in lines:
        if line.startswith("+") and not line.startswith("+++"):
            data = re.findall(r'[\u4e00-\u9fff]+', line)
            total_font += len("".join(data))
    print("find pr:{} and total is:{}".format(pr_number, total_font))
    return total_font


def get_pr(title):
    content = re.search(r"\[(\d+)\]", title)
    pr_number = content.group()[1:-1]
    return pr_number


def get_issue_by_status(token, status=open_status):
    open_issue_list = list()
    page = 0
    per_page = 100
    while True:
        request_url = url.format(token, status, page, per_page)
        ret = requests.get(request_url)
        issues = ret.json()
        for issue in issues:
            print("find index:{} data:{}".format(page, issue["title"]))
            if issue["title"].startswith("[Auto]"):
                pr_number = get_pr(issue["title"])
                total_size = get_total_font(pr_number)
                open_issue_list.append([issue["title"], issue["html_url"], status, total_size])
        if len(issues) == 0:
            break
        page += 1
    return open_issue_list


def write_csv(list_data):
    list_data.insert(0, ["title", "link", "status", "count"])
    with open("./result.csv", mode="w", newline="") as file:
        writer = csv.writer(file)
        writer.writerows(list_data)


@click.command()
@click.option("--token", required=True, help="Please input the token of gitee")
def get_issue_info(token):
    list_data = list()
    open_issues = get_issue_by_status(token, open_status)
    list_data.extend(open_issues)
    progress_issues = get_issue_by_status(token, progress_status)
    list_data.extend(progress_issues)
    print("the length of data:{}".format(len(list_data)))
    write_csv(list_data)


if __name__ == '__main__':
    get_issue_info()
