# README

### 1.介绍

本工具是为翻译sig自动提ISSUE的自动化脚本。

工具使用场景是：当gitee的pr合并后通过webhook触发CI门禁，在CI门禁里面执行脚本自动创建翻译的ISSUE.

### 2.工具使用

1.配置translation.yaml文件

2.执行脚本：python3 create_transation_issue.py owner repo token pr_number

### 3.脚本介绍：

create_transation_issue.py：自动创建issue脚本，针对于开源仓。

create_translation_issue_by_auth:  自动创建issue脚本， 针对于私有仓。

create_transation_issue_by_env:  自动创建issue脚本，针对于开源仓， 通过环境变量获取到对应的值。

create_transation_issue_by_exclude_weekly.py:  自动创建issue脚本，针对于开源仓, 排查以weekly为起始的分支。

scan_transation_font_count.py:  计算打开和正在处理中的issue, 统计对应PR的汉字数据，并将数据写在excel文档中。

count_repo_font_by_pr： 统计指定时间段范围内的汉字翻译字数。

### 4.配置介绍

translation： openharmony社区的配置

translation_ascend:  ascend社区的配置

### 5.配套使用

create_transation_issue_by_env + translation: openharmony社区使用

create_translation_issue_by_auth+ translation_ascend： ascend社区在使用





