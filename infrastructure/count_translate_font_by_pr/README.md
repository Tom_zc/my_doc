

# count_translate_font_by_pr

功能： 统计指定组织，指定仓库，指定分支，指定指定时间范围合入PR的统计

### 1.使用python命令执行

1.配置介绍：

~~~bash
token: xxxxxxxxxxxxxxxxxxxxxx        # gitee获取数据的token
owner: openharmony                   # 组织
repo: docs                           # 仓库
branch: master                       # 分支
path: zh-cn/                         # 指定路径
start-date: 2024-07-01 00:00:00      # 指定起始时间
end-date: 2024-07-31 00:00:00        # 查询指定结束时间
file_extension:                      # 查询的文件扩展名
  - extension: md
~~~

2.依赖包安装

~~~bash
pip3 install pyyaml click requests
~~~

3.脚本执行：

~~~bash
python3 count_translate_font_by_pr.py
~~~

### 2.使用exe文件执行

1.配置介绍

~~~bash
token: xxxxxxxxxxxxxxxxxxxxxx        # gitee获取数据的token
owner: openharmony                   # 组织
repo: docs                           # 仓库
branch: master                       # 分支
path: zh-cn/                         # 指定路径
start-date: 2024-07-01 00:00:00      # 指定起始时间
end-date: 2024-07-31 00:00:00        # 查询指定结束时间
file_extension:                      # 查询的文件扩展名
  - extension: md
~~~

token来源于gitee申请:

![1721963244387](assets/1721963244387.png)

2.双击[count_translate_font_by_pr.exe](dist/count_translate_font_by_pr.exe)

