#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# @Time    : 2024/7/24 14:32
# @Author  : Tom_zc
# @FileName: count_translate_font_by_pr.py
# @Software: PyCharm
import re
import datetime
import os
import time
import logging
import threading
from collections import defaultdict
from functools import wraps
from logging import handlers
from concurrent.futures import ThreadPoolExecutor, wait

import yaml
import click
import requests
import csv

_yaml_fields = ["owner", "repo", "branch", "path", "start-date", "end-date", "file_extension"]
_pr_info_url = "https://gitee.com/api/v5/repos/{}/{}/pulls?access_token={}&state=merged&base={}&" \
               "sort=created&direction=desc&page={}&per_page=100"
_diff_pr_url = "https://gitee.com/{}/{}/pulls/{}.diff"
_template_pr_url = "https://gitee.com/{}/{}/pulls/{}"

"""
_character_data = [
    '–', '—', '‘', '’', '“', '”', '…', '、', 
    '。', '〈', '〉', '《', '》', '「', '」', '『', 
    '』', '【', '】', '〔', '〕', '！', '（', '）',
    '，', '．', '：', '；', '？'
]
"""
_character_data = [
    r'\u3002', r'\uFF1F', r'\uFF01', r'\u3010', r'\u3011', r'\uFF0C', r'\u3001', r'\uFF1B',
    r'\uFF1A', r'\u300C', r'\u300D', r'\u300E', r'\u300F', r'\u2019', r'\u201C', r'\u201D',
    r'\u2018', r'\uFF08', r'\uFF09', r'\u3014', r'\u3015', r'\u2026', r'\u2013', r'\uFF0E',
    r'\u2014', r'\u300A', r'\u300B', r'\u3008', r'\u3009'
]

_font_re = re.compile(r'[\u4e00-\u9fff]+')
_character_re = re.compile(r'{}+'.format("|".join(_character_data)))


class Logger:
    """get the logger"""
    level_relations = {
        'debug': logging.DEBUG,
        'info': logging.INFO,
        'warning': logging.WARNING,
        'error': logging.ERROR,
        'crit': logging.CRITICAL
    }

    # noinspection SpellCheckingInspection
    def __init__(self, filename, level='info', when='D', back_count=3,
                 fmt='%(asctime)s - %(pathname)s[line:%(lineno)d] - %(levelname)s: %(message)s'):
        self._logger = logging.getLogger(filename)
        _format_str = logging.Formatter(fmt)
        self._logger.setLevel(self.level_relations.get(level))
        sh = logging.StreamHandler()
        sh.setFormatter(_format_str)
        self._logger.addHandler(sh)
        th = handlers.TimedRotatingFileHandler(filename=filename, when=when, backupCount=back_count, encoding='utf-8')
        th.setFormatter(_format_str)
        self._logger.addHandler(th)

    @property
    def logger(self):
        return self._logger


logger = Logger('./count_translate_font_by_pr.log').logger


def func_retry(retry=3, delay=1):
    """retry the function"""

    def deco_retry(fn):
        @wraps(fn)
        def inner(*args, **kwargs):
            for i in range(retry):
                try:
                    return fn(*args, **kwargs)
                except Exception as e:
                    logger.error("[func_retry] e:{}, args:{}, kwargs:{}".format(e, args, kwargs))
                    time.sleep(delay)
            else:
                raise RuntimeError("[func_retry] retry reached the number of failures")

        return inner

    return deco_retry


class CountResult:
    """save the result about translate count"""

    _lock = threading.Lock()
    _merged_lock = threading.Lock()
    _count_result = defaultdict(int)
    _date_result = defaultdict(tuple)

    @classmethod
    def update_result(cls, key, value):
        """
        :key: pr_number, path,
        :value: count
        """
        with cls._lock:
            if key in cls._count_result.keys():
                print("[update_result] find the duplicate key:{}".format(key))
            else:
                cls._count_result[key] += value

    @classmethod
    def update_merged_date(cls, pr_numer, date):
        """
        :key: merged_at, created_at,
        :value: count
        """
        with cls._merged_lock:
            cls._date_result[pr_numer] = date

    @property
    def result(self):
        return self._count_result

    @property
    def date_result(self):
        return self._date_result


@func_retry(delay=3)
def request_gitee(url, is_json=True):
    """request gitee info by url"""
    ret = requests.get(url, timeout=(300, 300))
    if ret.status_code != 200:
        logger.error("request_gitee failed, and ret code:{}".format(ret.status_code))
        return None
    if is_json:
        return ret.json()
    return ret.content


def get_pr_number(config_obj):
    """get all the pr number that satisfied the condition"""
    duration_prs = list()
    page = 0
    while True:
        is_over_query = False
        logger.info("[get_pr_number] start to collect the page:{}".format(page))
        request_url = _pr_info_url.format(config_obj["owner"],
                                          config_obj["repo"],
                                          config_obj["token"],
                                          config_obj["branch"],
                                          page)
        pr_list = request_gitee(url=request_url)
        if pr_list is None:
            logger.info("[get_pr_number] get pr info failed:{}".format(pr_list))
            raise RuntimeError("request pr info failed")
        if len(pr_list) == 0:
            return duration_prs
        for pr_info in pr_list:
            if pr_info["base"]["ref"] != config_obj["branch"]:
                logger.info("[get_pr_number] find the not branch:{}".format(pr_info["base"]["ref"]))
                continue
            merged_date = datetime.datetime.strptime(pr_info["merged_at"], "%Y-%m-%dT%H:%M:%S+08:00")
            create_date = datetime.datetime.strptime(pr_info["created_at"], "%Y-%m-%dT%H:%M:%S+08:00")
            logger.info("[get_pr_number] find the pr create-date:{}".format(create_date))
            if config_obj["start-date"] <= merged_date <= config_obj["end-date"]:
                CountResult.update_merged_date(pr_info["number"], (pr_info["merged_at"], pr_info["created_at"]))
                duration_prs.append(pr_info["number"])
            if create_date < config_obj["start-date"]:
                is_over_query = True
                break
        if is_over_query:
            return duration_prs
        page += 1


def count_font_by_pr_num(pr_number, file_extension, config_obj):
    """count the font number in pr diff file"""
    url = _diff_pr_url.format(config_obj["owner"], config_obj["repo"], pr_number)
    logger.info("[count_font_by_pr_num] start to collect the pr:{}".format(pr_number))
    diff_content = request_gitee(url, is_json=False)
    if diff_content is None:
        logger.info("[count_font_by_pr_num] get pr diff file failed:{}".format(pr_number))
        raise RuntimeError("request gitee diff failed")
    try:
        diff_content = diff_content.decode("utf-8")
    except UnicodeDecodeError as err:
        logger.error("[count_font_by_pr_num] find the pr number :{} that decode failed:{}".format(pr_number, err))
        diff_content = str(diff_content)
    diff_paragraph = diff_content.split("diff --git")
    for diff_info in diff_paragraph:
        if not diff_info:
            continue
        lines = diff_info.split("\n")
        path = lines[0].split("b/")[-1].strip()
        is_in_file_extension = path.split("/")[-1].split(".")[-1] in file_extension
        if path.startswith(config_obj["path"]) and is_in_file_extension:
            logger.info("[count_font_by_pr_num] find the filename:{}".format(path))
            total_font = 0
            for line in lines:
                if line.startswith("+") and not line.startswith("+++"):
                    all_data = _font_re.findall(line)
                    character_data = _character_re.findall(line)
                    all_data.extend(character_data)
                    total_font += len("".join(all_data))
            CountResult.update_result((pr_number, path), total_font)
            logger.info("[count_font_by_pr_num] find pr:{} and total front is:{}".format(pr_number, total_font))


def _parse_config(config_path):
    """read config"""
    with open(config_path, "r", encoding="utf-8") as f:
        return yaml.safe_load(f)


def _check_config(config_obj):
    """check config"""
    fields = list(set(_yaml_fields) - set(config_obj.keys()))
    if fields:
        raise RuntimeError("lack the fields of:{}".format(",".join(fields)))


def write_csv(config_obj):
    """write data to csv"""
    list_data = list()
    date_result = CountResult().date_result
    for key, value in CountResult().result.items():
        pr_link = _template_pr_url.format(config_obj["owner"], config_obj["repo"], key[0])
        tuple_date = date_result.get(key[0])
        list_data.append([key[0], pr_link, key[1], value, tuple_date[0], tuple_date[1]])
    list_data = sorted(list_data, key=lambda x: x[4], reverse=True)
    list_data.insert(0, ["PR", "Link", "Filename", "Count", "Merged_Date", "Create_Date"])
    cur_date = datetime.datetime.now().strftime("%Y_%m_%d_%H_%M_%S")
    with open("./count_translate_font_{}_{}.csv".format(config_obj["branch"], cur_date), mode="w", newline="") as file:
        writer = csv.writer(file)
        writer.writerows(list_data)


@click.command()
@click.option("--path", default="./config.yaml", help='The path of script config')
def main(path):
    config_path = os.getenv("CONFIG_PATH")
    if not config_path:
        config_path = path
    logger.info("1.start to parse config")
    config_obj = _parse_config(config_path)
    _check_config(config_obj)
    file_extension = [i["extension"] for i in config_obj["file_extension"]]
    logger.info("2.start to get pr number")
    pr_numbers = get_pr_number(config_obj)
    pr_numbers = list(set(pr_numbers))
    logger.info("3.find the pr number total:{}, and start to count front".format(len(pr_numbers)))
    executor = ThreadPoolExecutor(max_workers=50)
    all_tasks = [executor.submit(count_font_by_pr_num, pr_number, file_extension, config_obj)
                 for pr_number in pr_numbers]
    wait(all_tasks)
    logger.info("4.start to write csv")
    write_csv(config_obj)
    logger.info("done successfully")


if __name__ == '__main__':
    main()
