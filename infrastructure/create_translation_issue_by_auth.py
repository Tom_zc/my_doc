# -*- coding: utf-8 -*-
# @Time    : 2023/8/31 15:49
# @Author  : Tom_zc
# @FileName: create_translation_issue.py
# @Software: PyCharm
import os
import copy
import time
import traceback
import requests
import yaml
import sys
import re
from functools import wraps


def func_retry(retry=3, delay=1):
    def deco_retry(fn):
        @wraps(fn)
        def inner(*args, **kwargs):
            for i in range(retry):
                try:
                    return fn(*args, **kwargs)
                except Exception as e:
                    print("[func_retry] e:{}, args:{}, kwargs:{}".format(e, args, kwargs))
                    time.sleep(delay)
            else:
                raise RuntimeError("[func_retry] retry reached the number of failures")

        return inner

    return deco_retry


class GiteeRequest:
    request_open_issue_url = "https://gitee.com/api/v5/repos/{}/{}/issues?access_token={}&state=open&sort=created&direction=desc&page={}&per_page=100"
    request_processing_issue_url = "https://gitee.com/api/v5/repos/{}/{}/issues?access_token={}&state=progressing&sort=created&direction=desc&page={}&per_page=100"
    request_pr_diff_url = "https://gitee.com/{}/{}/pulls/{}.diff?access_token={}"
    request_pr_diff_url_by_auth = "https://gitee.com/api/v5/repos/{}/{}/pulls/{}/files?access_token={}"
    request_create_issue_url = "https://gitee.com/api/v5/repos/{}/issues"
    request_pr_info_url = "https://gitee.com/api/v5/repos/{}/{}/pulls/{}?access_token={}"
    request_issue_info_url = "https://gitee.com/api/v5/repos/{}/{}/pulls/{}/issues?access_token={}&page=1&per_page=100"
    request_modify_issue_url = "https://gitee.com/api/v5/repos/{}/issues/{}"

    def __init__(self, token):
        self.token = token
        self.headers = {
            "Content-Type": "application/json",
            "charset": "UTF-8"
        }

    @func_retry()
    def request_open_issue(self, owner, repo, page):
        url = self.request_open_issue_url.format(owner, repo, self.token, page)
        resp = requests.get(url=url, headers=self.headers)
        if not str(resp.status_code).startswith("2"):
            raise Exception("[create_translation_issue] request open issue return code:{}".format(resp.status_code))
        return resp.json()

    @func_retry()
    def request_progressing_issue(self, owner, repo, page):
        url = self.request_processing_issue_url.format(owner, repo, self.token, page)
        resp = requests.get(url=url, headers=self.headers)
        if not str(resp.status_code).startswith("2"):
            raise Exception(
                "[create_translation_issue] request progressing issue return code:{}".format(resp.status_code))
        return resp.json()

    @func_retry()
    def request_open_issue(self, owner, repo, page):
        url = self.request_open_issue_url.format(owner, repo, self.token, page)
        resp = requests.get(url=url, headers=self.headers)
        if not str(resp.status_code).startswith("2"):
            raise Exception("[create_translation_issue] request open issue return code:{}".format(resp.status_code))
        return resp.json()

    @func_retry()
    def request_pr_diff_by_auth(self, owner, repo, pr_id):
        url = self.request_pr_diff_url_by_auth.format(owner, repo, pr_id, self.token)
        resp = requests.get(url=url, headers=self.headers)
        if not str(resp.status_code).startswith("2"):
            raise Exception(
                "[create_translation_issue] request pr diff by auth return code:{}".format(resp.status_code))
        return resp.json()

    @func_retry()
    def request_pr_diff(self, owner, repo, pr_id):
        url = self.request_pr_diff_url.format(owner, repo, pr_id, self.token)
        resp = requests.get(url=url, headers=self.headers)
        if not str(resp.status_code).startswith("2"):
            raise Exception("[create_translation_issue] request pr diff return code:{}".format(resp.status_code))
        return resp.text

    @func_retry()
    def request_create_issue(self, owner, repo, pr_id, issue_title, body):
        param = {
            "access_token": self.token,
            "owner": owner,
            "repo": repo,
            "title": issue_title + "[{}]".format(pr_id),
            "issue_type": "翻译",
            "body": body
        }
        url = self.request_create_issue_url.format(owner)
        resp = requests.post(url, params=param)
        if not str(resp.status_code).startswith("2"):
            raise Exception("[create_translation_issue] request create issue return code:{}".format(resp.status_code))
        else:
            print("[create_translation_issue] create issue successfully, and issue number is:{}".format(
                resp.json().get("number")))
        return resp.json()

    @func_retry()
    def request_modify_issue(self, owner, repo, issue_id, assignee, collaborators):
        body = {
            "access_token": self.token,
            "repo": repo,
            "state": "progressing",
            "assignee": assignee,
        }
        if collaborators:
            body["collaborators"] = collaborators
        url = self.request_modify_issue_url.format(owner, issue_id)
        resp = requests.patch(url, json=body)
        if not str(resp.status_code).startswith("2"):
            raise Exception("[create_translation_issue] request modify issue return code:{}".format(resp.status_code))
        else:
            print("[create_translation_issue] modify issue successfully")
        return resp.text

    @func_retry()
    def request_pr_info(self, owner, repo, pr_id):
        url = self.request_pr_info_url.format(owner, repo, pr_id, self.token)
        resp = requests.get(url=url, headers=self.headers)
        if not str(resp.status_code).startswith("2"):
            raise Exception("[create_translation_issue] request pr info return code:{}".format(resp.status_code))
        return resp.json()

    @func_retry()
    def request_issue_info(self, url):
        resp = requests.get(url=url, headers=self.headers)
        if not str(resp.status_code).startswith("2"):
            raise Exception("[create_translation_issue] request issue info return code:{}".format(resp.status_code))
        return resp.json()

    @func_retry()
    def request_issue_info_by_auth(self, owner, repo, pr_id, token):
        url = self.request_issue_info_url.format(owner, repo, pr_id, token)
        resp = requests.get(url=url, headers=self.headers)
        if not str(resp.status_code).startswith("2"):
            raise Exception("[create_translation_issue] request issue info return code:{}".format(resp.status_code))
        return resp.json()


def load_yaml(file_path):
    with open(file_path, encoding="utf-8") as fp:
        content = yaml.load(fp.read(), Loader=yaml.Loader)
    return content


def get_open_issue(owner, repo, token):
    gitee_request = GiteeRequest(token)
    all_list_issue = list()
    page = 0
    while True:
        limit_issue = gitee_request.request_open_issue(owner, repo, page)
        print("[create_translation_issue] collect open issue:{}".format(len(limit_issue)))
        all_list_issue.extend(limit_issue)
        if len(limit_issue) == 100:
            page += 1
            continue
        break
    return all_list_issue


def get_progressing_issue(owner, repo, token):
    gitee_request = GiteeRequest(token)
    all_list_issue = list()
    page = 0
    while True:
        limit_issue = gitee_request.request_progressing_issue(owner, repo, page)
        print("[create_translation_issue] collect progressing issue:{}".format(len(limit_issue)))
        all_list_issue.extend(limit_issue)
        if len(limit_issue) == 100:
            page += 1
            continue
        break
    return all_list_issue


def get_pr_diff_files(owner, repo, number, token):
    diff_files_list = list()
    gitee_request = GiteeRequest(token)
    diff_content = gitee_request.request_pr_diff(owner, repo, number)
    diff_files = [x.split(' ')[0][2:] for x in diff_content.split('diff --git ')[1:]]
    for diff_file in diff_files:
        if diff_file.endswith('\"'):
            d = re.compile(r'/[\d\s\S]+')
            diff_file = d.findall(diff_file)
            diff_file = diff_file[0].replace('/', '', 1).replace('\"', '')
            diff_files_list.append(diff_file)
        else:
            diff_files_list.append(diff_file)
    return diff_files_list


def get_pr_diff_files_by_auth(owner, repo, number, token):
    diff_files_list = list()
    gitee_request = GiteeRequest(token)
    diff_content = gitee_request.request_pr_diff_by_auth(owner, repo, number)
    for content in diff_content:
        diff_files_list.append(content["filename"])
    return diff_files_list


def get_all_pr_id_from_issue():
    all_issue_list = list()
    open_issue = get_open_issue(pr_owner, pr_repo, access_token)
    all_issue_list.extend(open_issue)
    progressing_issue = get_progressing_issue(pr_owner, pr_repo, access_token)
    all_issue_list.extend(progressing_issue)
    pr_id_list = [i.get("title").split('.')[-1].replace('[', '').replace(']', '') for i in all_issue_list]
    return pr_id_list


def post_issue(owner, repo, pr_id, issue_title, body, token):
    gitee_request = GiteeRequest(token)
    resp = gitee_request.request_create_issue(owner, repo, pr_id, issue_title, body)
    return resp


def put_issue(owner, repo, issue_id, assignee, collaborators, token):
    gitee_request = GiteeRequest(token)
    resp = gitee_request.request_modify_issue(owner, repo, issue_id, assignee, collaborators)
    return resp


def get_pr_relate_issue_title(owner, repo, pr_id, token):
    gitee_request = GiteeRequest(token)
    resp = gitee_request.request_issue_info_by_auth(owner, repo, pr_id, token)
    title_list = [i["title"] for i in resp]
    return title_list


def render_html(need_translate_list):
    issue_sheet = '|files|assignee|\r\n|:--:|:--:|\r\n'
    list_lines = list()
    for conent in need_translate_list:
        line = "|{}|{}|\r\n".format(conent["path"], conent["assignee"])
        list_lines.append(line)
    content = issue_sheet + "".join(list_lines)
    return content


def get_max_length_path(path_dict):
    path_max_length, max_length_path = 0, str()
    for path in path_dict.keys():
        if len(path) > path_max_length:
            path_max_length = len(path)
            max_length_path = path
    return max_length_path


def handler(pr_owner, pr_repo, pr_number, access_token, config_content):
    repositories = config_content["repositories"]
    pr_id_list = get_all_pr_id_from_issue()
    if pr_number in pr_id_list:
        print("Error: issue has already created, please go to check issue: #{}".format(pr_number))
        raise RuntimeError("issue has already created")
    pr_relate_issue_titles = get_pr_relate_issue_title(pr_owner, pr_repo, pr_number, access_token)
    for pr_relate_issue_title in pr_relate_issue_titles:
        if pr_relate_issue_title.startswith("[Auto]"):
            print("The pr: #{} relate the issue exist".format(pr_number))
            return
    diff_files = get_pr_diff_files_by_auth(pr_owner, pr_repo, pr_number, access_token)
    print("[create_translation_issue] diff files path:{}".format(",".join(diff_files)))
    for repository in repositories:
        zh_dict = dict()
        need_translate_list, collaborators_list = list(), list()
        if pr_owner != repository["owner"]:
            continue
        if pr_repo != repository["repo"]:
            continue
        if repository["auto_create_issue"]:
            for diff_file in diff_files:
                if diff_file.startswith("en/"):
                    continue
                need_zh_path_dict = dict()
                for issue_trigger in repository["issue_triggers"]:
                    is_match_name = diff_file.startswith(issue_trigger["trigger_pr_path"])
                    file_extension = [i["extension"] for i in issue_trigger["file_extension"]]
                    is_match_suffix = diff_file.split('.')[-1] in file_extension
                    is_match_zh = "zh" in issue_trigger["trigger_pr_path"]
                    if is_match_name and is_match_suffix and is_match_zh:
                        need_zh_path_dict[issue_trigger["trigger_pr_path"]] = {
                            "sign_to": [i["sign_to"] for i in issue_trigger["assign_issue"]],
                            "file": diff_file
                        }
                if len(need_zh_path_dict):
                    zh_max_length_path = get_max_length_path(need_zh_path_dict)
                    zh_dict[zh_max_length_path] = copy.deepcopy(need_zh_path_dict.get(zh_max_length_path))
            print("[create_translation_issue] need translate path:{}".format(",".join(zh_dict.keys())))
            if len(zh_dict):
                for path, value in zh_dict.items():
                    need_translate_dict = dict()
                    need_translate_dict["path"] = value["file"]
                    assignee_list = ["@{}".format(i) for i in value["sign_to"]]
                    need_translate_dict["assignee"] = ",".join(assignee_list)
                    collaborators_list.extend(value["sign_to"])
                    need_translate_list.append(need_translate_dict)
            if len(need_translate_list):
                title = repository["title"]
                template = render_html(need_translate_list)
                body = "Related PR link: +https://gitee.com/{}/{}/pulls/{}\r\n{}".format(pr_owner, pr_repo,
                                                                                         pr_number, template)
                ret = post_issue(pr_owner, pr_repo, pr_number, title, body, access_token)
                collaborators_list = list(set(collaborators_list))
                collaborators_list = [i for i in collaborators_list if i]
                if len(collaborators_list) == 0:
                    continue
                elif len(collaborators_list) == 1:
                    assignee = collaborators_list[0]
                    collaborators = ""
                else:
                    assignee = collaborators_list[0]
                    collaborators = ",".join(collaborators_list[1:])
                put_issue(pr_owner, pr_repo, ret["number"], assignee, collaborators, access_token)


def main(owner, repo, number, token, path):
    try:
        config_content = load_yaml(path)
        handler(owner, repo, number, token, config_content)
    except Exception as e:
        print("[create_translation_issue] e:{}, traceback:{}".format(e, traceback.format_exc()))
        sys.exit(1)


if __name__ == '__main__':
    if len(sys.argv) != 5:
        print('Required 4 parameters! The pr_owner, pr_repo, pr_number and access_token '
              'need to be transferred in sequence.')
        sys.exit(1)
    pr_owner = sys.argv[1]
    pr_repo = sys.argv[2]
    pr_number = sys.argv[3]
    access_token = sys.argv[4]
    config_path = os.getenv("translation_config_path", "translation_ascend.yaml")
    main(pr_owner, pr_repo, pr_number, access_token, config_path)
