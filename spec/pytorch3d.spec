Name:		pytorch3d
Version:	0.7.6
Release:	6
Summary:	PyTorch3D is FAIR's library of reusable components for deep learning with 3D data
Group:		Applications/System
License:        BSD License

URL:		https://pytorch.org/
Source0:    https://github.com/facebookresearch/pytorch3d/archive/refs/tags/v%{version}.tar.gz

BuildRequires: python3-devel
BuildRequires: python3-setuptools
BuildRequires: python-pip
BuildRequires: torch


%description
PyTorch3D is FAIR's library of reusable components for deep learning with 3D data

%prep
%setup -q -c -n %{name}

%build
CFLAGS="$RPM_OPT_FLAGS" %{__python} setup.py build

%install
%{__python} setup.py install --skip-build --root %{buildroot} --install-scripts %{_bindir}


