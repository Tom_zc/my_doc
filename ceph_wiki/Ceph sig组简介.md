# 愿景

汇聚存储英才，共同打造高质量、高性能、高可靠性的ceph版本，构建丰富的南北向生态

# 目标

- 打造两层质量防护网，确保ceph在各个版本上高质量发布
- 及时解决和处理ceph相关的issue、pr和CVE，确保ceph的高质量按时发布
- 针对ARM架构开展优化，打造ceph极致性能
- 紧跟ceph原生社区发展，坚持“upstream first”原则，丰富openeuler ceph生态圈
- 根据用户诉求和ceph sig组成员共同决策openeuler新版本上ceph的版本

# 组织会议

北京时间，双周二上午10:00~11:00

# 链接
1. openEuler SDS sig组简介：https://gitee.com/openeuler/community/tree/master/sig/sig-SDS
2. SDS sig组bilibili会议视频归档：https://space.bilibili.com/527064077/search/video?keyword=sds
3. 邮件列表归档地址：https://mailweb.openeuler.org/hyperkitty/list/sds@openeuler.org/

# weixin 讨论群
请加liu_qinfei微信好友，拉你入群